Title: Ropa Austral inventariada en la sección
Date: 24/05/09

Os informamos que  después de entregar la ropa que nos llegó de Austral, nos ha sobrado el siguiente material y que por tanto lo tenéis a vuestra disposición para su compra sin necesidad de tener que comprarla por la web.

El inventario es el siguiente (aparece la Talla - Número de unidades disponibles):

| SUDADERA ALGODÓN AZUL MARINO | CAMISETA MC SERIGRAFÍA | BERMUDA BASIK | BERMUDA PRO HOMBRE | CAMISETA MC PRO 1.1 HOMBRE |
| ---------------------------- | ---------------------- | ------------- | ------------------ | -------------------------- |
| <del>XXS  - 1</del>                 | 8y - <del>8, </del> 7 | <del>XXS - 1</del> | <del>12y - 1</del> | <del>S - 1</del>      |
|                              | 12y - <del>4, 3</del> 2 | <del>S - 3, 2, 1</del> | <del>S - 3, 2, 1</del> | M -1        |
|                              | <del>S - 2, 1</del> | L - 1         | M - 1              |                            |
|                              | M - 1                  |               |                    |                            |
|                              | XL - 1                 |               |                    |                            |
|                              | XXL - 2                |               |                    |                            |

Si os interesa algo de lo siguiente mandarnos un correo a deleg.nat.cnh@gmail.com. Se atenderán las peticiones por orden de llegada al correo.

Por otra parte, os recordamos que el 1 de junio se abrirá el plazo para adquirir el material de austral por la web a precio reducido o promocionado (igual que el que os damos desde la sección). 



 

 