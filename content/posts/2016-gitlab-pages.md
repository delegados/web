Title: Reunión de presentación
Date: 2024-04-05
Category: Comunicados
Tags: Comunicados
Status: draft


# Presentación

* ¿Quiénes somos?
* Contacto: correo, presencialmente


# Situación actual

* Venimos de unos meses críticos
* Situación complicada:

   - Recursos fisicos limitados y anticuados
   - Dificultades  para encontrar entrenadores cuando se necesitan (se obvian razones)

* Pero hay esperanza en que esto no se hunda: Marcos está haciendo sobreesfuerzo y posiblemente tenga mucha culpa de que esté Marc

# Objetivos a corto plazo

* Devolver estabilidad a la sección **importante, nosotros  somos el pegamento entre entrenadores, nadadores, padres y junta. Evitar conflictos en caliente**
* Optimizar los recursos actuales
* Traje a medida (de nuestras posibilidades) a Marcos

# Objetivos a largo plazo - Carta a los reyes magos

* A la junta directiva
* Al cuerpo técnico
* A los padres: **CORDURA**, ¿Patrocinios serían bienvenidos?

# Agradecimiento 

* Muestras de apoyo recibidas
* Ruegos y preguntas: **Importante: No comprometerse más de la cuenta, somos nuevos**