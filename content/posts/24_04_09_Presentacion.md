Title: Presentación
Date: 24/04/09

# Zarpamos ...

Con este post inaguramos nuestra aventura como **delegados de la sección de natación del Centro Natación Helios**.

El grupo humano que ha decidido tomar las riendas del barco lo componen:

* Jesús Malón 
* Pepe Salueña 
* Carlos Rodríguez 

Afrontamos este proyecto después  de haber vivido unos meses complicados dentro de la sección por lo que no va a ser empresa fácil. Sin embargo, comenzamos con toda la ilusión del mundo.

Un saludo de parte de los delegados.