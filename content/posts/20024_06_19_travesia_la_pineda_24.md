Title: Travesía La Pineda 2024
Date: 2024/06/19

Buenos días.

Como comentamos hace unas semanas, el domingo 28 de julio es la Travesía a nado de La Pineda.  

Este es el formulario de inscripción:

https://docs.google.com/forms/d/e/1FAIpQLScL48X9jqPeN1BtvLdUi1MxOl9SBHRT1kjF29Sh6_VDS2qlAw/viewform?usp=sf_link


Para el año 2011 y menores la travesía es de 800m. mientras que para el año 2010 y mayores es de 2000m. En ambos casos hay premiación por categorías (https://cnvilaseca.com/). No obstante cualquiera puede hacer la de 800m en modalidad Open (cualquier edad), para la que no hay clasificación ni premiación.

Aunque existe la categoría prebenjamín, recomendamos la inscripción solo desde categoría benjamín (nacidos en 2014 y posteriores).

La inscripción incluye un donativo a la asociación SWIM for ELA y está abierta también a cualquier socio del club no federado, con un coste adicional por el seguro de día.

Todo el que termine la travesía tendrá entrada para AQUOPOLIS y para los acompañantes la organización facilita un descuento del 30%.

Desde la sección vamos a contratar servicio de autobús para el desplazamiento. En caso de cubrirse todas las plazas, tendrán prioridad los participantes federados por la sección y sus acompañantes (padres y hermanos si el participante es menor de edad) y por orden de recepción de formularios.

Horarios:

- Salida autobús desde Macanaz: 6:15
- Recogida dorsales: 9:30
- Inicio 800m: 10:30
- Inicio 2000m: 11:00
- Salida autobús desde la Pineda: 20:00 (llegada estimada a Macanaz: 23:00).

Precio:

- Inscripción federados con bus: 40€
- Inscripción federados sin bus;16€
- Inscripción no federados con bus: 50€ 
- Inscripción no federados sin bus:19€
- Acompañantes mayores de edad (con bus): 40€
- Acompañantes menores de edad (con bus): 20€

Forma pago: recibo domiciliado a la cuenta del primer participante. Se remesará tras el verano  junto a otros importes pendientes a la sección.

Escribidnos para cualquier duda.

Saludos,

Jesús, Pepe y Carlos